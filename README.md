Obverse Import Tool
===================

Description
-----------
The Obverse Import Tool provides a framework for importing data from an Excel spreadsheet to a data store.
It relies heavily on [PHPExcel][3] for parsing the Excel file, but then processes the file according to simple
rules specified in a yaml config file.

Requirements
------------
The requirements for this bundle are:

 * PHP version 5.3.2 or higher

And the requirements for [PHPExcel][2] upon which we rely heavily are:

 * PHP extension php_zip enabled *(this extension is needed for Excel 2007 (.xlsx) files)*
 * PHP extension php_xml enabled
 * PHP extension php_gd2 enabled


Installation
------------
1.  Install [Composer][1] if you don't already have it

        curl -s https://getcomposer.org/installer | php
    
    This will create a composer.phar file in your current directory.

2. Create a composer.json file with the following:

            {
            "require": {
                "obverse/php-excel": "dev-master",
                "obverse/import": "dev-master"
            },
            "repositories": [
                   {
                       "type": "composer",
                       "url": "http://www.obverse.com/packages"
                   }
               ]
            }

3.  Install the package

        php composer.phar install
    
    This will download the obverse directory to vendor/obverse and create a
    vendor/autoload.php for autoloading.

Usage
-----
1. Create an importer class that implements OBV\Import\Importers\ImporterInterface or use the sample one provided in the [Obverse Import Bundle][]

        <?php
        namespace Acme;
        use OBV\Import\Importers\AbstractImporter;
        use OBV\Import\Importers\ImporterInterface;
        
        class FooImporter extends AbstractImporter implements ImporterInterface
        {
            public function __construct()
            {
                // ...
            }
            public function importRow($row, $errors=array())
            {
                // ...
            }
            public function saveErrors($errors)
            {
                // ...
            }
            public function determineHeaderRow($data)
            {
                // ...
            }
            // also define any callback methods you may need
            // for validation/filtering/processing, etc ...
        }

2.  Create a yaml file with a '**table**' key indicating the table the data should
    be stored in and a '**cols**' key containing a list of the columns that data 
    will be stored in.  The order of the columns should correspond to the order
    of the columns in the spreadsheet.  In the example below, the first column
    of the spreadsheet will be assigned to the 'fname' variable, the second to 
    'lname', etc.  

    Under each column you may assign a '**required**' key to indicate that the 
    field is mandatory.  A row will not be saved if a mandatory field is left blank 
    in the spreadsheet.  You may also assign a '**callbacks**' key populated with 
    an array of callback functions to be performed on the cell.  Callback functions 
    must be a valid callable method defined within your importer class and can do 
    any filtering, validating, etc ...  Each callable must either throw an exception
    or return an array with a '**data**' key and an '**errors**' key.  See example.

        # contact_mapping.yml
        
        table: contacts
        cols:
            fname: 
                required: 1
            lname:
                required: 1
            email:
                required: 1
                callbacks: [validateEmail, lowercaseEmail]

3. Example usage

        <?php
        require '/path/to/vendor/autoload.php';
        use OBV\Import;
        use OBV\Import\Helpers\PhpExcelHelper;
        use Acme\FooImporter;
        
        # Here we use a PDO connection, but this can be whatever you wish
        $pdo = new PDO(...);
        $import = new Import(new PhpExcelHelper());
        $import->setImporter(new FooImporter($pdo, 'contact_mapping.yml'));
        $import->loadSpreadsheet($spreadsheet_filename);
        $results = $import->process();
        print_r($results);
        


[1]:  http://getcomposer.org
"Dependency Manager for PHP"

[2]:    http://phpexcel.codeplex.com/wikipage?title=Requirements&referringTitle=FAQ
"Requirements for PHPExcel"

[3]:  https://github.com/PHPOffice/PHPExcel
"A pure PHP Library for reading and writing spreadsheet files"

