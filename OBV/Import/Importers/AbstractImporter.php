<?php
namespace OBV\Import\Importers;

use OBV\Import\Importers\ImporterInterface;
use Symfony\Component\Yaml\Yaml;

class AbstractImporter implements ImporterInterface
{
    protected $dbh;
    protected $tableName;
    public $columnMapping;

    public function __construct(\PDO $dbh, $configFile)
    {
        $this->dbh = $dbh;
        $this->parseConfigFile($configFile);
    }

    /**
     * parseConfigFile
     * Parses config file, sets tableName and columnMapping
     * 
     * @access private
     * @return void
     * @throws InvalidArgumentException
     */
    public function parseConfigFile($configFile)
    {
        $mapping = Yaml::parse($configFile);
        if (!empty($mapping['table'])) {
            $this->setTableName($mapping['table']);
        } else {
            throw new \InvalidArgumentException('Invalid mapping file, table not specified');
        }
        if (!empty($mapping['cols'])) {
            $this->setColumnMapping($mapping['cols']);
        } else {
            throw new \InvalidArgumentException('Invalid mapping file, no cols specified.');
        }
    }

    public function importRow($row, $errors=array())
    {
    }

    public function saveErrors($errors)
    {
    }

    /**
     * determineHeaderRow 
     * 
     * @access public
     * @return NULL 
     * @throws RuntimeException
     */
    public function determineHeaderRow($data)
    {
        foreach ($data as $idx => $row) {
            if ($row[0] == 'First Name') {
                return $idx;
            }
        }
        throw new \RuntimeException('Unable to determine header row in spreadsheet');
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }
    public function getColumnMapping()
    {
        return $this->columnMapping;
    }
    public function setColumnMapping($columnMapping)
    {
        $this->columnMapping = $columnMapping;
    }

}
