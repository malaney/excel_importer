<?php
namespace OBV\Import\Importers;

interface ImporterInterface
{
    public function importRow($row, $errors=array());
    public function saveErrors($errors);
    public function determineHeaderRow($data);
}
