<?php
namespace OBV\Import\Helpers;

/**
 * PhpExcelHelper
 * Wrapper for PHPExcel static calls
 * 
 */
class PhpExcelHelper
{
    public function __construct() 
    {
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
    }

    /**
     * identify 
     * 
     * @param string $filename 
     * @access public
     * @return string
     */
    public function identify($filename)
    {
        return \PHPExcel_IOFactory::identify($filename);
    }
    
    /**
     * createReader 
     * 
     * @param string $fileType 
     * @access public
     * @return mixed
     */
    public function createReader($fileType)
    {
        return \PHPExcel_IOFactory::createReader($fileType);
    }
}
