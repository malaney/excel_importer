<?php
namespace OBV;

use OBV\Import\Callbacks;
use OBV\Import\Importers\ImporterInterface;

/**
 * Import 
 * 
 * @package 
 * @version $id$
 * @copyright 2012 Obverse Dev
 * @author Malaney J. Hill <malaney@gmail.com> 
 * @license PHP Version 3.01 {@link http://www.php.net/license/3_01.txt}
 */
class Import
{
    private $columnMapping;
    private $importer;
    private $data = array();
    private $headerRow = 0;
    
    /**
     * __construct 
     * 
     * @param string $type 
     * @param \Obverse\ImportBundle\Helpers\PhpExcelHelper $excelReader 
     * @param ImporterInterface $importer 
     * @access public
     * @return NULL
     */
    public function __construct(Import\Helpers\PhpExcelHelper $excelReader, ImporterInterface $importer=NULL)
    {
        $this->importer = $importer;
        $this->excelReader = $excelReader;
        if ($importer) {
            $this->setImporter($importer);
        }
    }

    /**
     * loadSpreadsheet 
     * 
     * @param string $spreadsheetFileName 
     * @access public
     * @return void
     */
    public function loadSpreadsheet($spreadsheetFileName)
    {
        $fileType = $this->excelReader->identify($spreadsheetFileName);
        $objReader = $this->excelReader->createReader($fileType);

        if (method_exists($objReader, 'setReadDataOnly')) {
            $objReader->setReadDataOnly(true);
        }
        $objPHPExcel = $objReader->load($spreadsheetFileName);

        $i=0;
        $data = array();
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false); 
                foreach ($cellIterator as $cell) {
                    $this->data[$i][] = $cell->getCalculatedValue();
                }
                $i++;
            }
        }
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }

    /**
     * process 
     * 
     * @access public
     * @return array
     */
    public function process()
    {
        $this->headerRow = $this->importer->determineHeaderRow($this->data);
        $errors = array();
        $totalRows = $importedRows = 0;
        foreach ($this->data as $idx => $row) {
            if ($idx > $this->headerRow) {
                // Filter out empty rows
                $filtered = array_filter(array_map('trim', $row));
                if (!empty($filtered)) {
                    $results = $this->processRow($row);
                    if (!empty($results['errors'])) {
                        if (empty($errors[$totalRows])) {
                            $errors[$totalRows] = array();
                        }
                        $errors[$totalRows] = array_merge($errors[$totalRows], $results['errors']);
                    }
                    if (!empty($results['success'])) {
                        $importedRows++;
                    }

                    $this->importer->saveErrors($results);
                    $totalRows++;
                }
            }
        }
        return array('errors' => $errors, 'totalRows' => $totalRows, 'importedRows' => $importedRows);
    }

    public function getImporter()
    {
        return $this->importer;
    }

    public function setImporter(ImporterInterface $importer)
    {
        $this->importer = $importer;
        $this->setMapping($this->importer->columnMapping);
    }

    /**
     * getMapping 
     * 
     * @access public
     * @return void
     */
    public function getMapping()
    {
        return $this->columnMapping;
    }

    /**
     * setMapping 
     * 
     * @param mixed $columnMapping 
     * @access public
     * @return void
     */
    public function setMapping($columnMapping)
    {
        $this->columnMapping = $columnMapping;
    }

    /**
     * processRow 
     * 
     * @param mixed $row 
     * @access public
     * @return void
     */
    public function processRow($row)
    {
        try {
            $mappedRow = $this->mapRow($row);
            return $this->importer->importRow($mappedRow['data'], $mappedRow['errors']);
        } catch (\PDOException $e) {
            throw $e;
        } catch (\RuntimeException $e) {
            return $this->importer->importRow(NULL, $mappedRow['errors']);
        }
    }

    /**
     * mapRow 
     * This function processes the mapping file. 
     * Throws exception if a field is marked as required and yet empty
     * Applies any callbacks to a given field.  Callback functions must
     * be defined in the importer class and must either throw an exception
     * or return an array:  array('data' => $data, 'errors' => array()
     * 
     * @param mixed $row 
     * @access public
     * @return array
     */
    public function mapRow($row)
    {
        $i = 0;
        $payload = $errors = array();
        try {
            foreach ($this->columnMapping as $key => $value) {
                // Check for required fields
                if (!empty($value['required'])) {
                    if (empty($row[$i])) {
                        throw new \InvalidArgumentException('Field [ '. $value . ' ' . $key . '] is required and we got [' . $row[$i]. ']');
                    }
                } 
                // Apply any callbacks
                if (!empty($value['callbacks'])) {
                    if (!is_array($value['callbacks'])) {
                        $value['callbacks'] = (array) $value['callbacks'];
                    }
                    foreach ($value['callbacks'] as $callback) {
                        if (is_callable(array($this->importer, $callback))) {
                            $result = call_user_func_array(array($this->importer, $callback), array($row[$i]));
                            if (!isset($result['data']) || !isset($result['errors'])) {
                                throw new \RuntimeException('Callback function must return an array with "data" and "errors" keys');
                            }
                            $payload[$key] = $result['data'];
                            $errors = array_merge($errors, $result['errors']);
                        } else {
                            throw new \RuntimeException('Unknown callback for field: ' . $callback);
                        }
                    }
                } else {
                    $payload[$key] = $row[$i];
                }
                $i++;
            }
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }
        return array('data' => $payload, 'errors' => $errors);
    }
}
