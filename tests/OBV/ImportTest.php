<?php

namespace OBV;

use OBV\Import\Helpers\PhpExcelHelper;
use OBV\TestContactImporter;
use OBV\ContainerAwareUnitTestCase;

class ImportTest extends ContainerAwareUnitTestCase
{
    public function setup()
    {

        $blank_row = array_fill(0,18, '');
        $headers = array('First Name', 'Last Name', 'Company Name', 'Gender', 'Status', 'Birthday', 'Mobile Phone', 'Office Phone', 'Direct Phone', 'Fax', 'Email', 'Email-2', 'Full Address', 'Skype', 'Facebook', 'Twitter', 'Website', 'Notes / Requirements');

        $data_row = array( 'John', 'Doe', 'Obverse', 'Male', '', '', '201-555-1212', '201-555-1212', '201-555-1212', '201-555-1212', 'john@doe.com', 'john@dev.doe.com', '18 Any Street, Anytown, NJ 90210', 'john.doe', 'johnny', 'johndoe', 'www.doe.com', 'my notes');

        $this->data[0] = $blank_row;
        $this->data[1] = $headers;
        $this->data[2] = $data_row;


        $this->excelHelper = new PhpExcelHelper(); 
        $this->importerInterface = new TestContactImporter($this->get('obverse_import.db'), $this->getParameter('obverse_import.importer_mapping_file'), 'reg_user');
    }
    
    public function testFirst()
    {
        $this->assertEquals('foo', 'foo');
    }

    /**
     * @expectedException ErrorException
     */
    public function testConstructorInvalidArgs()
    {
        $import = new \OBV\Import('foo', 'bar');
    }

    /**
     * @expectedException ErrorException
     */
    public function testConstructorInvalidNumArgs()
    {
        $import = new \OBV\Import('foo');
    }

    public function testConstructor3()
    {
        $import = new Import($this->excelHelper, $this->importerInterface);
        $this->assertTrue(is_object($import));
    }

    /**
     * @expectedException Exception
     */
    public function testNonExistentFile()
    {
        $import = new Import($this->excelHelper, $this->importerInterface);
        $import->loadSpreadsheet('non-existent file');
    }

    public function testInvalidFile()
    {
        $import = new Import($this->excelHelper, $this->importerInterface);
        $filename = 'foo.txt';
        touch($filename);
        try {
            $import->loadSpreadsheet($filename);
        } catch (\ErrorException $e) {
            unlink($filename);
            $this->assertTrue(true);
        }
    }
}
