<?php
namespace OBV;

use OBV\Import\Importers\AbstractImporter;

class TestContactImporter extends AbstractImporter
{
    public function __construct(\PDO $dbh, $mappingFile, $tableName, $envVars=array())
    {
        $this->dbh = $dbh;
        $this->setMappingFile($mappingFile);
        $this->envVars = $envVars;
        $this->tableName = $tableName;
        $this->setColumnMapping(); 
    }

    private function validateEnvVars()
    {
        if (empty($this->envVars['user_id'])) {
            throw new \RuntimeException('Missing required environment var [user_id]');
        }
        if (empty($this->envVars['country_id'])) {
            throw new \RuntimeException('Missing required environment var [country_id]');
        }
    }

    public function saveErrors($errors)
    {
        return true;
        // $dbClass = new \OBV\Import\DB($this->dbh);
        // $dbClass->saveContactErrors($errors);
    }

    public function importRow($row, $errors=array())
    {
        $newUserID = NULL;
        // Insert into db
        if (!empty($row)) {
            try {
                $this->dbh->beginTransaction();

                if (!empty($this->evnVars['country_id'])) {
                    $row['country_id'] = $this->envVars['country_id']; 
                }
                $row['owner_type'] = 'Individual';
                if (!empty($this->evnVars['user_id'])) {
                    $row['assigned_to'] = $this->envVars['user_id'];
                }
                $row['status'] = 1;
                $row['entered_by'] = "Import Script";
                $newUserID = $this->createRegUser($row);

                $this->dbh->commit();
            } catch (\PDOException $e) {
                $this->dbh->rollBack();
                $errors[] = $e->getMessage();
            }
        }
        return array('success' => !empty($newUserID), 'userID' => $newUserID, 'errors' => $errors);
    }

    public function validateEmail($email)
    {
        $email = trim($email);
        // see http://fightingforalostcause.net/misc/2006/compare-email-regex.php
        $emailValidatorRegex = '/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i';
        if (!preg_match($emailValidatorRegex, $email)) {
            throw new \InvalidArgumentException('Invalid email [' . $email . ']');
        }
        return array('data' => strtolower($email), 'errors' => array());
    }

    public function createRegUser($opts=array()) 
    { 
        $defaults = array(
                'fname' => '', 
                'lname' => '', 
                'email' => '',
                'mobile_phone' => '',
                'office_phone' => '',
                'direct_phone' => '',
                'assigned_to' => '',
                'status' => '',
                'display_name' => '',
                'owner_type' => '',
                'entered_by' => '',
                'facebook' => ''
                );

        $opts = array_merge($defaults, $opts);

        $sql = '
            INSERT INTO reg_user (
                    `fname`, 
                    `lname`, 
                    `email`, 
                    `mobile_phone`, 
                    `office_phone`, 
                    `direct_phone`, 
                    `assigned_to`, 
                    `status`,
                    `display_name`,
                    `date_added`,
                    `owner_type`,
                    `facebook`,
                    `skype`,
                    `twitter`,
                    `website`,
                    `comments`,
                    `gender`,
                    `full_address`,
                    `company_name`,
                    `entered_by`,
                    `birthday`,
                    `fax`,
                    `email_alt`
                    ) 
            VALUES (
                    :fname, 
                    :lname, 
                    :email, 
                    :mobile_phone, 
                    :office_phone, 
                    :direct_phone, 
                    :assigned_to, 
                    :status,
                    :display_name,
                    NOW(),
                    :owner_type,
                    :facebook,
                    :entered_by,
                    :skype,
                    :twitter,
                    :website,
                    :comments,
                    :gender,
                    :full_address,
                    :company_name,
                    :entered_by
                    :birthday,
                    :fax,
                    :email_alt
                   )
            ON DUPLICATE KEY UPDATE 
                reg_user_id=LAST_INSERT_ID(reg_user_id),
                status=VALUES(status),
                display_name=VALUES(display_name),
                owner_type=VALUES(owner_type),
                country_id=VALUES(country_id)
            ';
        $named_params = $this->_createNamedParams($opts);

        $sth = $this->dbh->prepare($sql);
        if (!$sth->execute($named_params)) {
            throw new RuntimeException('Bad insert ');
        }
        return $this->dbh->lastInsertID();
    }

    private function _createNamedParams($array)
    {
        $result = array();
        foreach ($array as $key => $val) {
            $result[':'.$key] = $val;
        }
        return $result;
    }
}
